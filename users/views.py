import logging

from django.conf import settings
from django.utils import timezone
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView

from restapi import restapi_response, success_messages, custom_error_codes
from restapi.permissions import IsAuthenticatedByUserItself
from restapi.success_messages import USER_PROFILE_UPDATED_SUCCESSFULLY_1106
from users import tasks
from users.models import User
from users.models import UserResetPassword, \
    UserVerification
from users.serializers import UserProfileGetSerializer, UserProfileSerializer
from users.serializers import UserSerializer, \
    UserRegistrationSerializer, UserLoginSerializer
from users.utils import create_reset_password_key

logger = logging.getLogger("restapi")



class UserRegistration(APIView):
    def post(self, request):
        """
            User login API

            *  Request sample for User registration/login request

                    {
                            "first_name" : string,
                            "last_name" : string,
                            "email" : string,
                            "password": string
                    }


            ** Success **


                {
                    "success": true,
                    "message": "User registration successful",
                    "payload": {
                        "user": {
                            "id": 6,
                            "first_name": "aniket",
                            "last_name": "Patel",
                            "email": "aniket45@yopmail.com",
                            "username": null,
                            "is_active": true,
                            "is_email_verified": false,
                            "created": "2018-07-02T10:15:33.799237Z",
                            "last_modified": "2018-07-02T10:15:33.906028Z",
                            "last_login": "2018-07-02T10:15:33.905223Z",
                            "user_role": null
                        },
                        "token": "837f83ac0675546dc1de8a8ee89451eae07ac82f"
                    }
                }


            ** Failure **


            *  Invalid request


                {
                    "success": false,
                    "message": "Bad request",
                    "payload": {
                        "email": [
                            "Enter a valid email address."
                        ]
                    },
                    "error_code": 400
                }

            *  Account already exists with same email


                {
                    "success": false,
                    "message": "Account with same email already exists",
                    "payload": {},
                    "error_code": 1002
                }
        """

        serializer = UserRegistrationSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                restapi_response.generate_failure_response(custom_error_codes.BAD_REQUEST_400,
                                                          payload=serializer.errors),
                status=status.HTTP_400_BAD_REQUEST)

        if User.objects.filter(email=request.data['email']).exists():
            return Response(
                restapi_response.generate_failure_response(custom_error_codes.ACCOUNT_ALREADY_EXISTS_1002,
                                                          payload=serializer.errors),
                status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.create_user(email=request.data['email'], password=request.data['password'])
        user.first_name = request.data['first_name']
        user.last_name = request.data['last_name']
        user.last_login = timezone.now()
        user.save()

        if settings.SEND_VERIFICATION_MAIN_ON_SIGNUP:
            tasks.send_verification_mail.delay(user.id)

        token, ok = Token.objects.get_or_create(user=user)

        user_serializer = UserSerializer(user)

        return Response(restapi_response.generate_response(
            success=True,
            msg=success_messages.USER_REGISTRATION_SUCCESSFUL_1100['message'],
            payload={
                'user': user_serializer.data,
                'token': token.key
            }),
            status=status.HTTP_201_CREATED)


class UserLogin(APIView):
    def post(self, request):
        """
            User login API

            Request sample for User login request

                    {
                            "email" : string,
                            "password": string
                    }


            ** Success


                {
                    "success": true,
                    "message": "User login successful",
                    "payload": {
                        "user": {
                            "id": 5,
                            "first_name": "aniket",
                            "last_name": "Patel",
                            "email": "aniket4@yopmail.com",
                            "username": null,
                            "is_active": true,
                            "is_email_verified": false,
                            "is_fb_linked": false,
                            "created": "2018-07-02T06:54:08.802778Z",
                            "last_modified": "2018-07-02T06:54:08.903597Z",
                            "last_login": "2018-07-02T06:54:08.902997Z",
                            "user_role": null
                        },
                        "token": "3a849e37812c763df5726248cf3d5d8063a8a46f"
                    }
                }
        """

        serializer = UserLoginSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                restapi_response.generate_failure_response(custom_error_codes.BAD_REQUEST_400,
                                                          payload=serializer.errors),
                status=status.HTTP_400_BAD_REQUEST)

        try:
            user = User.objects.get(email=request.data['email'])
            if user.check_password(request.data['password']):
                token = Token.objects.get(user_id=user.id)
                user_serializer = UserSerializer(user)
                return Response(restapi_response.generate_response(
                    success=True,
                    msg=success_messages.USER_LOGIN_SUCCESSFUL_1101['message'],
                    payload={
                        'user': user_serializer.data,
                        'token': token.key
                    }),
                    status=status.HTTP_200_OK)
            else:
                return Response(
                    restapi_response.generate_failure_response(custom_error_codes.ENTER_VALID_PASSWORD_1015,
                                                              payload={}), status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            return Response(
                restapi_response.generate_failure_response(custom_error_codes.EMAIL_NOT_REGISTERED_YET_1016,
                                                          payload={}), status=status.HTTP_400_BAD_REQUEST)


class UserEmailVerification(APIView):
    permission_classes = (IsAuthenticatedByUserItself,)

    def get(self, request, user_id, verification_key):
        """
        UserEmailVerification API

        Invalid token

                {
                        "success": false,
                        "message": "Invalid token.",
                        "payload": {},
                        "error_code": 401
                }
                HTTP status code 401:- Unauthorized

        You do not have permission to perform this action.(Token's user_id and request user_id not match)

            {
                    "payload": {},
                    "error_code": 403,
                    "message": "You do not have permission to perform this action.",
                    "success": false
            }
            HTTP status code 403:- Forbidden

        User does not exists

            {
                    "success" : false,
                    "message" : "User does not exists"
                    "error_code" : 7006
                    "payload" : {}

            }
            HTTP status code 400:- BAD REQUEST

        Invalid email verification key

            {
                    "success" : false,
                    "message" : "Invalid email verification key"
                    "error_code" : 1005,
                    "payload" : {}

            }
            HTTP status code 400:- BAD REQUEST

        Email verification key expired

            {
                    "success" : false,
                    "message" : "Email verification key expired"
                    "error_code" : 1004,
                    "payload" : {}

            }
            HTTP status code 400:- BAD REQUEST

        Pending email verification not found for this user

            {
                    "success" : false,
                    "message" : "Pending email verification not found for this user"
                    "error_code" : 1006,
                    "payload" : {}

            }
            HTTP status code 400:- BAD REQUEST

        Email verified successfully

            {
                    "success" : true,
                    "message" : "Email verified successfully"
                    "payload" : {
                        "user_id": integer
                    }

            }
            HTTP status code 200:- OK



        """
        user_verification = UserVerification.objects.filter(user_id=user_id)
        if request.user.id != int(user_id):
            return Response(
                restapi_response.generate_failure_response(custom_error_codes.UNAUTHORIZED_401,
                                                          payload={}),
                status=status.HTTP_401_UNAUTHORIZED)
        if user_verification.exists():
            user_verification = user_verification[0]
            if user_verification.key_expires < timezone.now():
                return Response(
                    restapi_response.generate_failure_response(custom_error_codes.EMAIL_VERIFICATION_KEY_EXPIRED_1004,
                                                              payload={}),
                    status=status.HTTP_400_BAD_REQUEST)
            else:
                if user_verification.verification_key == verification_key:
                    user = request.user
                    # removed use of is_email_verified flag
                    # user.is_email_verified = True

                    # change status of email in the email_status table
                    user.is_email_verified = True
                    user.save()

                    payload = {"user_id": user.id}
                    return Response(
                        restapi_response.generate_success_response(success_messages.EMAIL_VERIFIED_SUCCESSFULLY_1102,
                                                                  payload=payload),
                        status=status.HTTP_200_OK
                    )
                else:
                    return Response(
                        restapi_response.generate_failure_response(
                            custom_error_codes.INVALID_EMAIL_VERIFICATION_KEY_1005,
                            payload={}),
                        status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.PENDING_EMAIL_VERIFICATION_NOT_FOUND_FOR_THIS_USER_1006, payload={}),
                status=status.HTTP_400_BAD_REQUEST)


class UserPasswordResetRequest(APIView):
    def post(self, request, **kwargs):
        """
            **Password Reset**
            to reset user password.

            ### POST

            Send mail to the user on specified email address with the link to
            reset password.

            * Requires only the `email` address.

            * Possible HTTP status codes and JSON response:

                * `HTTP status code 400` - If user with specified email is not found.

                        {
                                "success" : false,
                                "message" : "User with specified email does not exist"
                                "error_code" : 1008,
                                "payload" : {}

                        }

                * `HTTP_400_BAD_REQUEST` - If email is not specified.


                        {
                                "success" : false,
                                "message" : "Email is required for password reset request"
                                "error_code" : 1007,
                                "payload" : {}

                        }

                * `HTTP_200_OK` - When Password Reset Link is successfully sent.

                        {
                                "success" : true,
                                "message" : "Password reset link sent successfully"
                                "payload" : {}

                        }
        """
        if 'email' not in request.data:
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.EMAIL_IS_REQUIRED_FOR_PASSWORD_RESET_REQUEST_1007, payload={}),
                status=status.HTTP_400_BAD_REQUEST)
        email = request.data['email']
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.USER_WITH_GIVEN_EMAIL_DOEST_NOT_EXISTS_1008, payload={}),
                status=status.HTTP_400_BAD_REQUEST)

        key = create_reset_password_key(user)
        tasks.send_reset_password_mail.delay(user.id, key)
        return Response(
            restapi_response.generate_success_response(success_messages.PASSWORD_RESET_LINK_SENT_SUCCESSFULLY_1103,
                                                      payload={}),
            status=status.HTTP_200_OK
        )


class UserChangePassword(APIView):
    permission_classes = (IsAuthenticatedByUserItself,)

    def post(self, request, user_id):
        """
        ** User new password set **

        ### Request

            {
                "new_password": string
            }

        ### Success

            {
                "message": "Password changed successfully !"
            }

        """

        if 'new_password' not in request.data:
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.REQUIRED_FIELD_MISSING_1001, payload={}),
                status=status.HTTP_400_BAD_REQUEST)
        new_password = request.data['new_password']
        user = request.user

        if user.id != int(user_id):
            return Response(restapi_response.generate_failure_response(
                payload={},
                error_message=custom_error_codes.USER_DOES_NOT_EXIST_1205),
                status=status.HTTP_403_FORBIDDEN)

        if new_password is "" or new_password is None:
            return Response(restapi_response.generate_failure_response(
                payload={},
                error_message=custom_error_codes.USER_NEW_PASSWORD_IS_NOT_ACCEPTABLE_1015),
                status=status.HTTP_400_BAD_REQUEST)

        if True:
            user = User.objects.get(id=user.id)
            user.set_password(new_password)
            user.save()
            tasks.send_password_changed_success_mail.delay(user_id)
            return Response(
                restapi_response.generate_success_response(success_messages.USER_PASSWORD_CHANGED_SUCCESSFULLY_1107,
                                                          payload={}),
                status=status.HTTP_200_OK
            )



class UserPasswordResetStatus(APIView):
    def get(self, request, user_id, key):
        """
        ** User Password reset key verfication **

        ### Success

            {
                "success": true,
                "message": "Password reset link is valid",
                "payload": {
                    "is_valid": true
                }
            }

        ### Failure

            {
                "success": false,
                "message": "Password reset request not found",
                "payload": {},
                "error_code": 1009
            }


            * If link is invalid or expired

            {
                "success": false,
                "message": "Password reset link expired",
                "payload": {},
                "error_code": 1010
            }


        """
        try:
            user_reset_password = UserResetPassword.objects.get(user_id=user_id)
        except UserResetPassword.DoesNotExist:
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.PASSWORD_RESET_REQUEST_NOT_FOUND_1009, payload={}),
                status=status.HTTP_400_BAD_REQUEST)

        if user_reset_password.key_expires < timezone.now():
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.PASSWORD_RESET_LINK_EXPIRED_1010, payload={}),
                status=status.HTTP_400_BAD_REQUEST)
        if user_reset_password.is_used is True:
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.PASSWORD_RESET_LINK_ALREADY_USED_1010, payload={}),
                status=status.HTTP_400_BAD_REQUEST)
        else:
            if user_reset_password.key == key:
                return Response(
                    restapi_response.generate_success_response(success_messages.PASSWORD_RESET_KEY_IS_VALID_1104,
                                                              payload={'is_valid': True}),
                    status=status.HTTP_200_OK
                )
            else:
                return Response(restapi_response.generate_failure_response(
                    custom_error_codes.PASSWORD_RESET_LINK_EXPIRED_1010, payload={}),
                    status=status.HTTP_400_BAD_REQUEST)


class UserUpdatePassword(APIView):
    def put(self, request, user_id):
        """
        ** User new password set **

        ### Request

            {
                "new_password": string,
                "key": string
            }

        ### Success

            {
                "message": "Password changed successfully !"
            }

        ### Failure

            {
                "success": false,
                "message": "Password reset request not found",
                "payload": {},
                "error_code": 1009
            }

            * If link is invalid or expired

            {
                "success": false,
                "message": "Password reset link expired",
                "payload": {},
                "error_code": 1010
            }


        """
        if 'key' not in request.data:
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.KEY_IS_REQUIRED_TO_UPDATE_PASSWORD_1011, payload={}),
                status=status.HTTP_400_BAD_REQUEST)
        key = request.data['key']

        if 'new_password' not in request.data:
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.NEW_PASSWORD_IS_REQUIRED_TO_UPDATE_PASSWORD_1012, payload={}),
                status=status.HTTP_400_BAD_REQUEST)
        new_password = request.data['new_password']

        try:
            user_reset_password = UserResetPassword.objects.get(user_id=user_id)
        except UserResetPassword.DoesNotExist:
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.PASSWORD_RESET_REQUEST_NOT_FOUND_1009, payload={}),
                status=status.HTTP_400_BAD_REQUEST)

        if user_reset_password.key_expires < timezone.now():
            return Response(restapi_response.generate_failure_response(
                custom_error_codes.PASSWORD_RESET_LINK_EXPIRED_1010, payload={}),
                status=status.HTTP_400_BAD_REQUEST)
        else:
            if user_reset_password.key == key:
                user = User.objects.get(id=user_id)
                user.set_password(new_password)
                user.save()
                user_reset_password.is_used = True
                user_reset_password.save()
                tasks.send_password_changed_success_mail.delay(user_id)
                return Response({"message": "Password changed successfully !"}, status=status.HTTP_200_OK)

            else:
                return Response(restapi_response.generate_failure_response(
                    custom_error_codes.PASSWORD_RESET_LINK_EXPIRED_1010, payload={}),
                    status=status.HTTP_400_BAD_REQUEST)


class CheckInternetAvailability(APIView):
    """
    To get status "online" if user make xhr request to test internet is working or not.

    /meta/is_online

              {
                "success": true,
                "message": "Internet is working",
                "payload": {"status": "online"}
             }
              HTTP status code 200:- OK
    """

    def get(self, request):
        return Response(restapi_response.generate_success_response(
            payload={"status": "online"},
            response_message=success_messages.META_USER_INTERNET_IS_WORKING_2005))



class UserGetPut(APIView):
    permission_classes = (IsAuthenticatedByUserItself,)

    # get api for fetching user profile details
    def get(self, request, user_id):
        """
            User Profile Get API

            User profile details fetched

                {
                        "payload" : {
                            "id" : integer,
                            "email" : string,
                            "first_name" : string,
                            "last_name" : string,
                            "profile_picture_url":url
                            "is_confirmed" : boolean,
                            "is_email_verified" : boolean,
                            "created": date time,
                        },
                        "success" : true,
                        "message" : "User profile details fetched"
                }
                HTTP status code 200:- OK

        """
        serializer = UserProfileGetSerializer(instance=request.user)

        logger.info("User profile details fetched successfully",
                    extra={'extra': {
                        'user_id': request.user.id
                    }})
        return Response(
            restapi_response.generate_response(
                success=True,
                msg="User profile details fetched",
                payload=serializer.data),
            status=status.HTTP_200_OK)

    # put api for updating user profile details
    def put(self, request, user_id):
        """
            User Profile Put API

            Request sample for User profile request

                {
                        "first_name" : string,
                        "last_name" : string,
                        "email" : string,
                        "profile_picture_url": url,
                        "is_confirmed": boolean
                }

            Invalid token

                {
                        "success": false,
                        "message": "Invalid token.",
                        "payload": {},
                        "error_code": 401
                }
                HTTP status code 401:- Unauthorized

            You do not have permission to perform this action.(Token's user_id and request user_id not match)

                {
                        "payload": {},
                        "error_code": 403,
                        "message": "You do not have permission to perform this action.",
                        "success": false
                }
                HTTP status code 403:- Forbidden


            Invalid data for user profile (Invalid email)

                {
                    "success": false,
                    "message": "Bad request",
                    "payload": {
                        "email": [
                            "Enter a valid email address."
                        ]
                    },
                    "error_code": 400
                }
                HTTP status code 400:- BAD REQUEST

            Invalid data for user profile (fields are blank)

                {
                    "success": false,
                    "message": "Bad request",
                    "payload": {
                        "first_name": [
                            "This field may not be blank."
                        ]
                    },
                    "error_code": 400
                }
                HTTP status code 400:- BAD REQUEST


            Email can not be null

                {
                    "success": false,
                    "message": "user email can not be null",
                    "payload": {},
                    "error_code": 1013
                }

            Profile updated successfully

                {
                    "success": true,
                    "message": "User profile updated successfully",
                    "payload": {
                        "id": 3,
                        "first_name": "aniket",
                        "last_name": "Patel",
                        "email": "boom@yopmail.com",
                        "is_email_verified": false,
                        "is_confirmed": false,
                        "profile_picture_url": "https://zo-albums.s3.amazonaws.com/81662688-a939-406a-8a2a-f7fc9254b50a.jpg",
                        "created": "2018-08-06T11:56:11.114772Z"
                    }
                }
                HTTP status code 200:- OK

        """

        # check request data is valid or not
        serializer = UserProfileSerializer(data=request.data, instance=request.user)
        # email not null validation
        if 'email' in request.data:
            if request.data['email'] is None:
                return Response(restapi_response.generate_failure_response(custom_error_codes.USER_EMAIL_CAN_NOT_BE_NULL,
                                                                          {}),
                                status=status.HTTP_400_BAD_REQUEST)
        # request data is not valid
        if not serializer.is_valid():
            # custom_error_codes.INVALID_DATA_1004
            return Response(restapi_response.generate_failure_response(custom_error_codes.BAD_REQUEST_400,
                                                                      serializer.errors),
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            user = serializer.save()

            serializer = UserProfileGetSerializer(instance=user)

            return Response(
                restapi_response.generate_success_response(USER_PROFILE_UPDATED_SUCCESSFULLY_1106, serializer.data),
                status=status.HTTP_200_OK)
