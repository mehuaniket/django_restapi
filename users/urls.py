from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from users import views

"""
    chatai/users URL Configuration
"""

urlpatterns = [
    url(r'^users/register/$', views.UserRegistration.as_view()),
    url(r'^users/login/$', views.UserLogin.as_view()),
    url(r'^users/(?P<user_id>[0-9]+)/$',
        views.UserGetPut.as_view()),
    url(r'^users/(?P<userid>[0-9]+)/verify/(?P<verification_key>\w+)/$',
        views.UserEmailVerification.as_view()),
    url(r'^users/(?P<user_id>[0-9]+)/change_password/$',
        views.UserChangePassword.as_view()),
    url(r'^users/password_reset_request/$', views.UserPasswordResetRequest.as_view()),
    url(r'^users/(?P<user_id>[0-9]+)/password_reset_status/(?P<key>\w+)/$', views.UserPasswordResetStatus.as_view()),
    url(r'^users/(?P<user_id>[0-9]+)/update_password/$', views.UserUpdatePassword.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
