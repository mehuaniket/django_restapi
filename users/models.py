from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.db import models
from django.utils import timezone

from commons.general_constants import DEFAULT_USER_PROFILE_AVATAR


class UserRole(models.Model):
    """
        Maintains roles available in the system
    """

    # name of the user role
    name = models.CharField(max_length=20)


class UserManager(BaseUserManager):
    """
        User manager class to handle user creation
    """

    def create_user(self, email, password=None, **extra_fields):
        """
            Creates a new User
              - Normalizes the email
              - Also creates a new auth token for the user
        """

        # Check if email is provided
        if not email:
            raise ValueError('User must have a valid email')

        # Normalize the provided email
        email = self.normalize_email(email)

        # Creating user object
        user = self.model(email=email, is_active=True, **extra_fields)
        # # setting user password
        user.set_password(password)
        # # saving user in database
        user.save()

        # Creating auth token for the user
        # token = Token.objects.create(user=user)
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        """
            Creates a superuser
        """
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self.create_user(email, password, **extra_fields)


class User(AbstractBaseUser):
    """
        Maintain user and its attributes
    """

    class Meta:
        db_table = 'user'

    # first name of the user
    first_name = models.CharField(max_length=200, null=True)
    # last name of the user
    last_name = models.CharField(max_length=200, null=True)
    # email id of the user
    email = models.EmailField(max_length=200, null=True, unique=True)
    # profile picture url
    profile_picture_url = models.TextField(null=True, default=DEFAULT_USER_PROFILE_AVATAR)
    # username of the user
    username = models.CharField(max_length=200, null=True)
    # role of the user (foreign key to UserRole model)
    user_role = models.ForeignKey(
        UserRole, on_delete=models.CASCADE, null=True)
    # indicates if the user is active or not
    is_active = models.BooleanField(default=True)
    # indicates if the user's email is verified or not
    is_email_verified = models.BooleanField(default=False)
    # the date when the user was created
    created = models.DateTimeField(default=timezone.now)
    # the date when the user object was last modified
    last_modified = models.DateTimeField(auto_now=True)
    # the date when the user last logged in
    last_login = models.DateTimeField(null=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    # user confirmation flag
    is_confirmed = models.BooleanField(default=False)

    # defines the user manager class for User
    objects = UserManager()

    # specifies the field that will be used as username by django
    # drf_auth_users framework
    USERNAME_FIELD = 'email'

    def get_full_name(self):

        """
            Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def has_perm(self, perm, obj=None):
        return self.is_superuser

    def has_module_perms(self, app_label):
        return self.is_superuser


class UserVerification(models.Model):
    class Meta:
        db_table = 'user_verification_code'

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    verification_key = models.CharField(max_length=100, blank=True)
    key_expires = models.DateTimeField()


class UserResetPassword(models.Model):
    class Meta:
        db_table = 'user_reset_password'

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    key = models.CharField(max_length=100, blank=True)
    key_expires = models.DateTimeField()
    is_used = models.BooleanField(default=False)


class UserContentActions(models.Model):
    """
             maintains the actions performed by user on content 
    """

    class Meta:
        db_table = 'user_content_action'

    # the date when entity was created
    timestamp = models.DateTimeField(default=timezone.now)
    # by which user the action has been performed
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    # which action has been performed
    # action = models.ForeignKey('content.ContentAction', on_delete=models.CASCADE, null=False)
    # the date when entity was created
    created = models.DateTimeField(default=timezone.now)
    # the date when the entity was last modified
    last_modified = models.DateTimeField(auto_now=True)


