import logging

from rest_framework import serializers

from users.models import User

logger = logging.getLogger('restapi')


class UserRegistrationSerializer(serializers.Serializer):
    """
        Serializer for user registration request data
    """

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    email = serializers.EmailField(max_length=256)
    first_name = serializers.CharField(allow_blank=True)
    last_name = serializers.CharField(allow_blank=True)
    password = serializers.CharField(max_length=256)




class UserLoginSerializer(serializers.Serializer):
    """
        Serializer for user registration request data
    """

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    email = serializers.EmailField(max_length=256)
    password = serializers.CharField(max_length=256)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('password',)


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'profile_picture_url', 'is_confirmed')


class UserProfileGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
            'email',
            'is_email_verified',
            'is_confirmed',
            'profile_picture_url',
            'created')
