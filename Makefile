stop-service:
	service nginx stop & service postgresql stop & service rabbitmq-server stop

build:
	docker-compose build

run-local:
	docker-compose -f docker-compose-local.yml up

restart-local:
	docker-compose -f docker-compose-local.yml restart

logs:
	docker-compose logs -f ${container}

bash:
	docker-compose exec web /bin/bash

stop-all-docker:
	docker stop $$(docker ps -a -q)


