 FROM python:3.5
 ENV PYTHONUNBUFFERED 1


 RUN mkdir /code
 WORKDIR /code
 RUN pip install --upgrade pip
 RUN apt-get -y update && apt-get -y install nginx

 ADD requirements.txt /code/
 RUN pip3 install -r requirements.txt
 ADD . /code/
 RUN chmod +x docker-entrypoint.sh
 RUN chmod +x docker-entrypoint-local.sh
 RUN chmod +x entrypoint_celery_worker.sh
 RUN chmod +x beat-entrypoint.sh
 ADD nginx/nginx.conf /etc/nginx/