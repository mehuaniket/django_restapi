#!/bin/sh

./wait-for-it.sh postgres:5432
#pip3 install -r requirements.txt
python3 manage.py makemigrations
python3 manage.py migrate
python3 restapi/setup_db.py
python3 manage.py collectstatic --noinput

python3 manage.py runserver 0.0.0.0:8000
# uwsgi --http :8000 --module restapi.wsgi
# uwsgi --ini uwsgi.ini

