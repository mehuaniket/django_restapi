# -*- coding: utf-8 -*-
import logging

from rest_framework import status
from rest_framework.exceptions import APIException
# Django imports
from rest_framework.response import Response
from rest_framework.views import exception_handler

# Initialize logger
from restapi import restapi_response, custom_error_codes

# Application imports
logger = logging.getLogger('charestapi')


def custom_exception_handler(exc, context):
    """
        Handles DRF's APIException and returns standard response
    """
    # if not isinstance(exc, APIException):
    if not type(exc) is APIException:
        response = exception_handler(exc, context)
        return Response(
            restapi_response.generate_response(
                success=False,
                msg=response.data['detail'],
                err_code=response.status_code,
            ),
            status=response.status_code)
    logger.debug(type(exc))
    # Generate restapi's standard response
    response = restapi_response.generate_response(
        success=False,
        msg=custom_error_codes.INTERNAL_SERVER_ERROR_500.get('message'),
        payload={},
        err_code=custom_error_codes.INTERNAL_SERVER_ERROR_500.get('err_code'))

    return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# RestapiCustomException
class RestapiCustomException(Exception):
    """
    Base class for restapi custom exceptions.
    """
    http_status_code = None
    message = None
    err_code = None
    payload = None

    def __init__(
            self,
            http_status_code=None,
            message=None,
            err_code=None,
            payload=None):
        self.payload = payload
        self.http_status_code = http_status_code
        self.message = message
        self.err_code = err_code

    def __str__(self):
        return self.message

