import datetime
import logging
import time

from django.utils.timezone import get_current_timezone

from users.models import User

logger = logging.getLogger("restapi")


def check_user_is_paid(user):
    return user.is_paid_user



def convert_epoch_to_timestampz(epoch_time):
    return datetime.datetime.fromtimestamp(
        int(epoch_time)).replace(
        tzinfo=get_current_timezone())


def convert_timestamp_to_epoch(date_object):
    return int(time.mktime(date_object.timetuple()))


def convert_datetime_string_to_datetime_obj(datetime_string):
    return datetime.datetime.strptime(
        datetime_string, '%Y-%m-%dT%H:%M:%SZ'
    )


def update_user_is_paid(user_id, is_paid):
    logger.debug("marking user %s as is_paid = %s" % (user_id, is_paid))
    User.objects.filter(id=user_id).update(is_paid_user=is_paid)


def extract_emails(email_string):
    """
    Extract all valid emails from the given string
    """
    if '@' not in email_string:
        return []
    emails = []

    def append_single_email(email):
        if 'not-applicable' in email:
            return
        email = email.strip('.').replace('..', '.')
        for case in [0, -1]:
            while not email[case].isdigit() and not email[case].isalpha():
                email = email[1:] if case == 0 else email[:-1]

        emails.append(email)

    email_string = email_string.replace(' dot ', '.')
    email_list = email_string.split()
    for i, e1 in enumerate(email_list):
        if e1.count('@') > 1:
            for e2 in e1.replace('|', '/').split('/'):
                e2 = e2.replace('@@', '@')
                if e2.count('@') > 1 and e2.count('..') > 1:
                    for e3 in e2.split('..'):
                        if e3.count('@') == 1:
                            append_single_email(e3)
                elif e2.count('@') == 1:
                    append_single_email(e2)
        elif '@' in e1:
            if e1[0] == '@' and i != 0:
                e1 = email_list[i - 1] + e1
            j = i
            email_condition = ('@' in e1 and '.' in e1.split('@')[1])
            while j != len(email_list) - 1 and (not email_condition or e1[-1] == '.'):
                e1 = e1 + email_list[j + 1]
                j += 1
            if '@' in e1 and '.' in e1.split('@')[1] and e1[0] != '@':
                append_single_email(e1)
    return list(set(emails))

def get_s3_bucket_url_by_new_image_name(old_image_url, new_image_name, old_image_name):
    return old_image_url.replace(old_image_name, new_image_name)
