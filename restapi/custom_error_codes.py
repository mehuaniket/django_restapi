# message digest for error responses

# others
ERR_CODE = "err_code"
MSG = "message"

# general
INTERNAL_SERVER_ERROR_500 = {
    ERR_CODE: 500,
    MSG: "Something went wrong. Please try again after sometime."}
FORBIDDEN_403 = {
    ERR_CODE: 403,
    MSG: 'You do not have permission to perform this action.'
}
UNAUTHORIZED_401 = {
    ERR_CODE: 401,
    MSG: 'Unauthorized to perform action'
}
BAD_REQUEST_400 = {
    ERR_CODE: 400,
    MSG: 'Bad request'
}

# custom genral errors
#  error messages to be shown when data is invalid
INVALID_DATA_1000 = {
    ERR_CODE: 1000,
    MSG: "Invalid request data"
}

INVALID_PAGE_NUMBER_1001= {
    ERR_CODE: 1001,
    MSG: "Invalid page number"
}


USER_DOES_NOT_EXIST_1205 = {
    ERR_CODE: 1205,
    MSG: 'User does not exist.'
}

ACCOUNT_WITH_USERNAME_NOT_FOUND_1210 = {
    ERR_CODE: 1210,
    MSG: 'No account found for the given username'
}

# custom Auth error codes range (1001 - 1100)

REQUIRED_FIELD_MISSING_1001 = {
    ERR_CODE: 1001,
    MSG: "required field(s) is/are missing"
}

ACCOUNT_ALREADY_EXISTS_1002 = {
    ERR_CODE: 1002,
    MSG: "This email is already registered."
}

INVALID_EMAIL_OR_PASSWORD_1003 = {
    ERR_CODE: 1003,
    MSG: 'Invalid email or password'
}

EMAIL_VERIFICATION_KEY_EXPIRED_1004 = {
    ERR_CODE: 1004,
    MSG: 'Email verification key expired'
}

INVALID_EMAIL_VERIFICATION_KEY_1005 = {
    ERR_CODE: 1005,
    MSG: 'Invalid email verification key'
}

PENDING_EMAIL_VERIFICATION_NOT_FOUND_FOR_THIS_USER_1006 = {
    ERR_CODE: 1006,
    MSG: 'Pending email verification not found for this user'
}

EMAIL_IS_REQUIRED_FOR_PASSWORD_RESET_REQUEST_1007 = {
    ERR_CODE: 1007,
    MSG: 'Email is required for password reset request'
}

USER_WITH_GIVEN_EMAIL_DOEST_NOT_EXISTS_1008 = {
    ERR_CODE: 1008,
    MSG: 'User with specified email does not exist'
}

PASSWORD_RESET_REQUEST_NOT_FOUND_1009 = {
    ERR_CODE: 1009,
    MSG: 'Password reset request not found'
}

PASSWORD_RESET_LINK_EXPIRED_1010 = {
    ERR_CODE: 1010,
    MSG: 'Password reset link expired.'
}

PASSWORD_RESET_LINK_ALREADY_USED_1010 = {
    ERR_CODE: 1010,
    MSG: 'Password reset link already used.'
}

KEY_IS_REQUIRED_TO_UPDATE_PASSWORD_1011 = {
    ERR_CODE: 1011,
    MSG: 'Key is required to update password'
}

NEW_PASSWORD_IS_REQUIRED_TO_UPDATE_PASSWORD_1012 = {
    ERR_CODE: 1012,
    MSG: "'new_password' is required to update password"
}

ENTER_VALID_EMAIL_1014={
    ERR_CODE: 1014,
    MSG: "Please enter a valid email address."
}

ENTER_VALID_PASSWORD_1015={
    ERR_CODE: 1015,
    MSG: "Sorry, this password isn’t right."
}
EMAIL_NOT_REGISTERED_YET_1016={
    ERR_CODE: 1016,
    MSG: "This email is not yet registered."
}
USER_EMAIL_CAN_NOT_BE_NULL = {
    ERR_CODE: 1013,
    MSG: 'user email can not be null'
}

USER_PASSWORD_DOES_NOT_MATCH_1014 = {
    ERR_CODE: 1014,
    MSG: "provided current password is not correct."
}

USER_NEW_PASSWORD_IS_NOT_ACCEPTABLE_1015 = {
    ERR_CODE:1015,
    MSG: "new password should not be blank/None"
}

#
ACCESS_TOKEN_EXPIRED_8011 = {
    ERR_CODE: 8011,
    MSG: "Access-Token expired please re-login"
}
ACCESS_TOKEN_EXPIRED_MSG_FROM_FB = {
    ERR_CODE: 190,
    MSG: 'Access token expired'
}
INVALID_USER_ID_FROM_FB = {
    ERR_CODE: 110,
    MSG: 'Invalid User ID'
}

FAILURE_IN_DEBUGGING_ACCESS_TOKEN_8012 = {
    ERR_CODE: 8012,
    MSG: 'Failure in debugging access token'
}

INVALID_FB_TOKEN_OR_FB_USER_ID_8003 = {
    ERR_CODE: 8003,
    MSG: "Facebook access_token or user_id  seems invalid."
}

INVALID_FB_USER_DATA_8004 = {
    ERR_CODE: 8004,
    MSG: "Invalid fb user data."
}

