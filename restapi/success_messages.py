# message digest for successful response messages
MSG_CODE = "message_code"
MSG = "message"

# general

RESOURCE_MODIFIED_SUCCESSFULLY_0001 = {
    MSG_CODE: 2001, MSG: "Resource modified successfully"}
RESOURCE_FETCHED_SUCCESSFULLY_0002 = {
    MSG_CODE: 2002, MSG: "Resource retrieved successfully"}
RESOURCE_CREATED_SUCCESSFULLY_0003 = {
    MSG_CODE: 2003, MSG: "Resource created successfully"}

# user module sucess messages 2000 to 2099

USER_REGISTRATION_SUCCESSFUL_1100 = {
    MSG_CODE: 1100,
    MSG: 'User registration successful'
}

USER_LOGIN_SUCCESSFUL_1101 = {
    MSG_CODE: 1101,
    MSG: 'User login successful'
}

EMAIL_VERIFIED_SUCCESSFULLY_1102 = {
    MSG_CODE: 1102,
    MSG: 'Email verified successfully'
}

PASSWORD_RESET_LINK_SENT_SUCCESSFULLY_1103 = {
    MSG_CODE: 1103,
    MSG: 'Password reset link sent successfully'
}

PASSWORD_RESET_KEY_IS_VALID_1104 = {
    MSG_CODE: 1104,
    MSG: 'Password reset link is valid'
}

FB_ACCOUNT_CONNECTED_SUCCESSFULLY_1105 = {
    MSG_CODE: 1105,
    MSG: 'Fb account connected successfully'
}

USER_PROFILE_UPDATED_SUCCESSFULLY_1106 = {
    MSG_CODE: 1106,
    MSG: "User profile updated successfully"
}

USER_PASSWORD_CHANGED_SUCCESSFULLY_1107 = {
    MSG_CODE: 1107,
    MSG: "Password changed successfully"
}

META_USER_INTERNET_IS_WORKING_2005 = {
    MSG_CODE: 2004,
    MSG: "Internet is working"
}
