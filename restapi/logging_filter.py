import json


class RestapiExtraLogFilter:
    """
    This class is a logger filter
    We have used a filter 'restapiextralogfilter' for console logging.
    This filter will accept the keyword 'extra' in the 'extra' argument of log if specified.
    If no keyword 'extra' it defaults to None.
    Also prints the log to console with 5 indentation to prettify the look of log.
    """

    def filter(
            self, record):
        if 'extra' not in record.__dict__:
            record.__dict__['extra'] = None
        try:
            record.__dict__['extra'] = json.dumps(
                record.__dict__['extra'], sort_keys=True, indent=5)
        except BaseException:
            record.__dict__['extra'] = "Failed to serialize to JSON"

        # Returning True, else django would raise Exception for invalid
        # argument extra
        return True
