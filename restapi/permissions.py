from rest_framework import permissions


class IsAuthenticatedByUserItself(permissions.BasePermission):

    def has_permission(self, request, view):
        # check if valid token has been provided
        if view.kwargs.get('user_id', None) is not None:
            return request.user.is_authenticated and request.user.id == int(view.kwargs['user_id'])
        return request.user.is_authenticated
