import os

import django

if __name__ == '__main__' and __package__ is None:
    os.sys.path.append(
        os.path.dirname(
            os.path.dirname(
                os.path.abspath(__file__))))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "restapi.settings")
django.setup()
from users.models import User


def create_superuser():
    try:
        user = User.objects.create_superuser(email='restadmin@yopmail.com', password='restapi')
        user.is_staff = True
        user.save()
    except:
        pass

def main():
    create_superuser()


main()
