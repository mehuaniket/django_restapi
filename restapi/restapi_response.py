#!/usr/bin/env python
# -*- coding: utf-8 -*-


def generate_response(
        success: object,
        msg: object,
        payload: object = {},
        err_code: object = None) -> object:
    """
        Generates json response
    """
    response_json = {'success': success, 'message': msg, 'payload': payload}

    if not success:
        if err_code:
            response_json['error_code'] = err_code
    return response_json


def generate_pagination_response(
        success: object,
        msg: dict,
        payload: object = {},
        next_page_number: object = None,
        previous_page_number: object = None) -> object:
    """
        Generates json response
    """
    response_json = {
        'success': success,
        'message': msg['message'],
        'payload': payload,
        'next_page': next_page_number,
        'previous_page': previous_page_number}

    return response_json


def generate_success_response(response_message: dict, payload={}):
    """
        Generates response for successful operations
    """
    return generate_response(
        success=True,
        msg=response_message['message'],
        payload=payload)


def generate_success_response_with_count(response_message: dict, payload={}, count: int = 0):
    """
        Generates response for successful operations
    """
    response = {
        'success': True,
        'message': response_message['message'],
        'payload': payload,
        'count': count
    }
    return response


def generate_failure_response(error_message: dict, payload={}):
    """
        Generate response in case of error
    """
    return generate_response(success=False,
                             msg=error_message['message'],
                             payload=payload,
                             err_code=error_message['err_code'])


