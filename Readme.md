# Django Rest api Example

This project includes basic skeleton to create REST Api using django and DRF. It includes all 
the basic Apis in `users`. It also includes swagger integration, celery worker integration. It has celery task examples, 
basic email templates and utility functions to send email using three platforms. 

### To RUN

- create .env file from .envtemplate and modify the varaibles as needed.
- 
```
DEBUG=True

DATABASE_USER=postgres
DATABASE_PASSWORD=postgres
DATABASE_HOST=postgres
DATABASE_PORT=5432
DATABASE_NAME=restapi
DATABASE_ENGINE=django.db.backends.postgresql

EMAIL_USERNAME=something@something.com
EMAIL_HOST=smtp.gmail.com
EMAIL_BACKEND=django.core.mail.backends.smtp.EmailBackend
EMAIL_PORT=587
EMAIL_HOST_USER=something@something.com
EMAIL_HOST_PASSWORD=************

SENDGRID_API_KEY=XXXXXXXXXX

SEND_EMAIL_VIA=gmail

FRONTEND_DOMAIN=http://localhost:4200
STATIC_ROOT=./static/
CELERY_BROKER_URL=amqp://myuser:mypassword@rabbitmq:5672//
CACHE_URL=memcached:11211

MEMCACHE_LOCATION=memcached:11211
AWS_DEFAULT_REGION=us-west-2

AWS_ACCESS_KEY=XXXXXXXXXXXXXXXXX
AWS_ACCESS_SECRET=XXXXXXXXXXXXXXXXXXXXXXX/XXXXXXXXXXXX
```

- I already made `Makefile` for basic commands. So, You can run following command to run this project. 
we are using docker-compose for our project.

```bash
make run-local
```

- To get logs for specific service.

```bash
make logs container=SERVICENAME
```

- To stop all other docker container before running this project.

```bash
make stop-all-docker
```

